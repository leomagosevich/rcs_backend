package controlenviadas

import (
	"database/sql"
	"fmt"
	"strconv"
)

func ControlEnviadas(db *sql.DB, idCamp string) (int, error) {

	var cantLin int

	idC, err := strconv.Atoi(idCamp)

	if err != nil {
		fmt.Printf("Error al convertir el idCamp a int: %v\n", err)
		return 0, err
	}

	sentenciaSQL := fmt.Sprintf("SELECT count(1) FROM enviados a WHERE a.Bid=%d", idC)

	filas, err := db.Query(sentenciaSQL)

	if err != nil {
		return 0, err
	}

	defer filas.Close()

	for filas.Next() {
		err := filas.Scan(&cantLin)

		if err != nil {
			return 0, err
		}

	}

	return cantLin, nil
}
