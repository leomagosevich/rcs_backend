package obtenerfechaactual

import (
	"fmt"
	"time"
)

func ObtenerFechaActual() (string) {
	t := time.Now()
	fecha := fmt.Sprintf("%d-%02d-%02d %02d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())

	return fecha
}

func ObtenerDiaActualLog() (string) {
	t := time.Now()
	fecha := fmt.Sprintf("%d%02d%02d", t.Year(), t.Month(), t.Day())

	return fecha
}
