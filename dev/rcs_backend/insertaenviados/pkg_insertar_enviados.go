package insertaenviados

import (
	"database/sql"
)

func InsertarEnviados(db *sql.DB, IdC int, linea int, estadoInsert int, estadoAgente int) error {

	sentenciaSQL := "INSERT INTO enviados (Bid, Line, Send, St) VALUES (?, ?, ?, ?)"

	insertaSQL, err := db.Prepare(sentenciaSQL)

	if err != nil {
		return err
	}

	defer insertaSQL.Close()

	_, err = insertaSQL.Exec(IdC, linea, estadoInsert, estadoAgente)

	if err != nil {
		return err
	}

	return nil

}
