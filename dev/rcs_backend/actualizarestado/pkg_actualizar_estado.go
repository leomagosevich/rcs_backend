package actualizarestado

import (
	"database/sql"
)

func ActualizaEstadoCamp(db *sql.DB, IdC string, estado int) error {

	sentenciaSQL := "UPDATE control set sent = ? WHERE BId = ?"

	actualizaSQL, err := db.Prepare(sentenciaSQL)

	if err != nil {
		return err
	}

	defer actualizaSQL.Close()

	_, err = actualizaSQL.Exec(estado, IdC)

	if err != nil {
		return err
	}

	return nil

}
