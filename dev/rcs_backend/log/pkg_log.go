package log

import (
	"fmt"
	"os"
	"rcs_backend/obtenerfechaactual"
)

func Logueo(mensaje string) error {
	var mascaraNombre string
	var mascaraFecha string
	var extLog string
	//var pathLog string
	var nombreLog string

	mascaraNombre = "rcs_backend_"
	mascaraFecha = obtenerfechaactual.ObtenerDiaActualLog()
	extLog = ".log"
	//pathLog = "./logs/"
	pathLog := os.Getenv("RCS_PATH_LOG")
	nombreLog = pathLog + mascaraNombre + mascaraFecha + extLog

	//fmt.Printf("Archivo Log: %s\n", nombreLog)

	//Si el archivo existe, se abre como solo lectura y para agregar contenido. Si no existe, se crea.
	f, err := os.OpenFile(nombreLog, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)

	defer f.Close()

	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return err
	}

	defer f.Close()

	_, err = f.WriteString(mensaje + "\n")

	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}

	f.Sync()

	return nil

}
