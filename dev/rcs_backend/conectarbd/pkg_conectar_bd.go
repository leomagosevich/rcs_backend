package conectarbd

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func ConectarBD(usu string, pass string, host string, base string) (db *sql.DB, e error) {

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@%s/%s", usu, pass, host, base))

	if err != nil {
		return nil, err
	}

	return db, nil

}
