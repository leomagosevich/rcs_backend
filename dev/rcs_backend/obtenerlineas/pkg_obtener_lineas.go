package obtenerlineas

import "database/sql"

type Linea struct {
	Bid   int
	Linea int
}

func ObtenerLineas(db *sql.DB, idC string) ([]Linea, error) {

	lineas := []Linea{}

	sentenciaSQL := "SELECT a.Bid, a.Line FROM base a WHERE a.Bid=" + idC + " AND NOT EXISTS(SELECT 1 FROM enviados b WHERE b.Bid = a.Bid AND b.Line = a.Line)"

	filas, err := db.Query(sentenciaSQL)

	if err != nil {
		return nil, err
	}

	defer filas.Close()

	var l Linea

	for filas.Next() {
		err := filas.Scan(&l.Bid, &l.Linea)

		if err != nil {
			return nil, err
		}

		lineas = append(lineas, l)
	}

	return lineas, nil

}
