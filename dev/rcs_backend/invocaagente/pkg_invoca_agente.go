package invocaagente

import (
	"database/sql"
	"fmt"
	"runtime"
	"sync"

	"net/http"
	"net/url"
	"os"
	"rcs_backend/insertaenviados"
	"rcs_backend/log"
	"rcs_backend/obtenerfechaactual"
	"rcs_backend/obtenerlineas"
	"strconv"
	"strings"
)

var wg sync.WaitGroup

func agente(db *sql.DB, dirUrl string, estadoInsertaEnv int, i int, idCamp int, lineas []int) {

	defer wg.Done()

	data := url.Values{}
	data.Set("BId", fmt.Sprintf("%d", idCamp))

	for _, linea := range lineas {

		data.Set("phone_number", fmt.Sprintf("%d", linea))
		metodo := "POST"
		client := &http.Client{}

		r, err := http.NewRequest(metodo, dirUrl, strings.NewReader(data.Encode()))

		if err != nil {
			fmt.Printf("Error al crear request con la línea %d\n", linea)
		}

		r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		r.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

		resp, err := client.Do(r)
		

		//mensaje := "Estado de la invocación " + resp.Status
		

		if err != nil {
			fmt.Printf("Linea %d: %s - statuscode: %d - error: %v\n", linea, resp.Status, resp.StatusCode , err)
		} /* else {
			fmt.Printf("Linea %d: %s - statuscode: %d\n", linea, mensaje, estadoAg)
		}*/

		defer resp.Body.Close()

		estadoAg := resp.StatusCode

		//Inserta en enviados
		err = insertaenviados.InsertarEnviados(db, idCamp, linea, estadoInsertaEnv, estadoAg)

		if err != nil {
			fmt.Printf("Error al insertar la línea %d en tabla enviados: %v\n", linea, err)
		} /* else {
			fmt.Printf("Se insertó correctamente la línea %d en tabla enviados\n", linea)

		}*/
	}

}

func InvocaAgente(db *sql.DB, dirUrl string, idCamp string, lineas []obtenerlineas.Linea, cantLineas int, concurrencia bool, estadoInsertaEnv int) (int, error) {

	var corte int
	var cantProcesos int
	cpu := runtime.NumCPU()
	//cpu := 2
	runtime.GOMAXPROCS(cpu)

	var lin []int
	var lin_tot []int

	var cantProcesadas int

	cantProcesadas = 0

	idC, err := strconv.Atoi(idCamp)

	if err != nil {
		fmt.Printf("Error al convertir el idCamp a int: %v\n", err)
		return 0, err
	}

	if concurrencia {

		if cantLineas < cpu {
			corte = 1
			cantProcesos = cantLineas
		} else {
			corte = cantLineas / cpu
			cantProcesos = cpu
		}
	} else {
		corte = cantLineas
		cantProcesos = 1
	}

	fecha := obtenerfechaactual.ObtenerFechaActual()
	err = log.Logueo(fmt.Sprintf("%s Invoca Agente: Concurrencia: %t - Cantidad de procesos: %d - Corte: %d", fecha, concurrencia, cantProcesos, corte))

	if err != nil {
		fmt.Printf("Error al escribir en el log - %v\n", err)
		os.Exit(2)
	}

	fmt.Printf("Invoca Agente: Concurrencia: %t - Cantidad de procesos: %d - Corte: %d\n", concurrencia, cantProcesos, corte)
	wg.Add(cantProcesos)

	for _, l := range lineas {
		lin_tot = append(lin_tot, l.Linea)
	}

	for i := 0; i < cantProcesos; i++ {

		switch i {
		case 0:
			lin = lin_tot[0:corte]
		case cantProcesos - 1:
			lin = lin_tot[corte*i : cantLineas]
		default:
			lin = lin_tot[corte*i : corte*(i+1)]
		}

		go agente(db, dirUrl, estadoInsertaEnv, i, idC, lin)

	}

	wg.Wait()
	return cantProcesadas, nil

}
