package obteneragente

import "database/sql"

type Agente struct {
	Bid     int
	AgentId int
	Logica  string
	Address string
}

func ObtenerAgente(db *sql.DB, idC string) ([]Agente, error) {

	agente := []Agente{}

	sentenciaSQL := "select c.BId, a.AgentId, a.Logica, a.Address from control c, agentes a where c.BId=" + idC + " and a.AgentId = c.AgentId"

	filas, err := db.Query(sentenciaSQL)

	if err != nil {
		return nil, err
	}

	defer filas.Close()

	var a Agente

	for filas.Next() {
		err := filas.Scan(&a.Bid, &a.AgentId, &a.Logica, &a.Address)

		if err != nil {
			return nil, err
		}

		agente = append(agente, a)
	}

	return agente, nil

}
