package main

import (
	//"errors"
	"fmt"
	"os"
	"rcs_backend/actualizarestado"
	"rcs_backend/conectarbd"
	"rcs_backend/controlenviadas"
	"rcs_backend/invocaagente"
	"rcs_backend/log"
	"rcs_backend/obteneragente"
	"rcs_backend/obtenerfechaactual"
	"rcs_backend/obtenerlineas"
	"strconv"
)

func main() {

	idCamp := os.Args[1]
	usuario := os.Args[2]
	passwd := os.Args[3]
	host := os.Args[4]
	base := os.Args[5]
	concurrencia, err := strconv.ParseBool(os.Args[6])

	if err != nil {
		fmt.Printf("RCS_BACKEND - Error al evaluar la concurrencia: %v\n", err)
		os.Exit(2)
	}

	var mensaje_err string
	var urlAgente string
	var fecha string

	err = log.Logueo("---------------------------------------------------------------------------------------------------------------")

	if err != nil {
		fmt.Printf("Error al escribir en el log - %v\n", err)
		os.Exit(2)
	}

	fecha = obtenerfechaactual.ObtenerFechaActual()
	err = log.Logueo(fmt.Sprintf("%s Ejecuta Campaña %s - concurrencia: %t", fecha, idCamp, concurrencia))

	if err != nil {
		fmt.Printf("Error al escribir en el log - %v\n", err)
		os.Exit(2)
	}

	estadoInsertaEnv := 1

	//Conecto BD
	db, err := conectarbd.ConectarBD(usuario, passwd, host, base)

	if err != nil {
		fmt.Printf("Error al conectar la BD - %v\n", err)
		os.Exit(2)
	}

	defer db.Close()

	//Obtengo el agente para la campaña recibida
	agente, err := obteneragente.ObtenerAgente(db, idCamp)

	if err != nil {
		fmt.Printf("Error al obtener el agente - %v\n", err)
		os.Exit(2)
	}

	//valida que se exista un agente asociado a la campaña
	if len(agente) == 0 {
		fmt.Printf("No se pudo obtener el agente asociado a la campaña %s\n", idCamp)
		os.Exit(2)
	}

	for _, agent := range agente {

		urlAgente = agent.Address
	}

	// Obtengo las líneas cargadas para la campaña recibida
	lineas, err := obtenerlineas.ObtenerLineas(db, idCamp)

	//valida que existan líneas asociadas a la campaña
	cantLineas := len(lineas)
	if cantLineas == 0 {
		mensaje_err = fmt.Sprintf("No se pudo obtener el listado de líneas a la campaña %s\n", idCamp)
		err2 := log.Logueo(mensaje_err)
		if err2 != nil {
			fmt.Printf("Error al escribir en el log - %v\n", err2)
			os.Exit(2)
		}
		fmt.Printf("Campaña %s - Sin líneas a procesar\n", idCamp)
		os.Exit(2)
	}

	if err != nil {
		fmt.Printf("Error al obtener listado de líneas - %v\n", err)
		os.Exit(2)
	}

	//Invoco al agente
	resultadoInvocaAgente, err := invocaagente.InvocaAgente(db, urlAgente, idCamp, lineas, cantLineas, concurrencia, estadoInsertaEnv)

	fmt.Printf("Resultado Invoca Agente: %v\n", resultadoInvocaAgente)

	if err != nil {
		mensaje_err = fmt.Sprintf("Error al invocar agente")
		err2 := log.Logueo(mensaje_err)
		if err2 != nil {
			fmt.Printf("Error al escribir en el log - %v\n", err2)
			os.Exit(2)
		}
		fmt.Printf("Campaña %s - Error al invocar agente\n", idCamp)
		os.Exit(2)
	}

	//Actualizo el estado de la campaña (2: Enviada)
	err = actualizarestado.ActualizaEstadoCamp(db, idCamp, 2)
	fecha = obtenerfechaactual.ObtenerFechaActual()

	if err != nil {
		mensaje_err = fmt.Sprintf("%s Error al actualizar el estado de la campaña en la tabla control", fecha)
		err2 := log.Logueo(mensaje_err)
		if err2 != nil {
			fmt.Printf("Error al escribir en el log - %v\n", err2)
			os.Exit(2)
		}
		fmt.Printf("%s Campaña %s - Error al actualizar el estado de la campaña en la tabla control\n", fecha, idCamp)
	} else {
		log.Logueo(fmt.Sprintf("%s Se actualizó correctamente el estado de la campaña %s", fecha, idCamp))

		if err != nil {
			fmt.Printf("Error al escribir en el log - %v\n", err)
			os.Exit(2)
		}
	}

	fecha = obtenerfechaactual.ObtenerFechaActual()

	cantidad, err := controlenviadas.ControlEnviadas(db, idCamp)

	if err != nil {
		fmt.Printf("Error al obtener cantidad de líneas enviadas - %v\n", err)
		os.Exit(2)
	}

	log.Logueo(fmt.Sprintf("%s Finaliza Campaña %s - Total de líneas enviadas: %d", fecha, idCamp, cantidad))

	if err != nil {
		fmt.Printf("Error al escribir en el log - %v\n", err)
		os.Exit(2)
	}

	fmt.Printf("RCS_BACKEND - Finaliza Campaña %s - Cantidad de líneas procesadas: %d\n", idCamp, cantidad)

}
