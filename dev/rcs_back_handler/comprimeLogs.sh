#!/bin/sh

cd /home/local/repo-rcs/logs/rcs_backend

FECHA=`date --date "yesterday" '+%Y%m%d'`

DIRLOGS=`pwd`
echo $DIRLOGS

DIRHISTO=$DIRLOGS'/historicos'

#ARCHIVO=ls | grep date '+%Y%m%d' | grep .log

echo "MASCARA LOG: "$FECHA

ARCHIVO=`ls | grep $FECHA | grep .log`

CANTARCH=`ls | grep $FECHA | grep .log | wc -l`

echo "CANTIDAD DE ARCHIVOS A COMPRIMIR: "$CANTARCH

if test $CANTARCH -eq 1 ; then
	echo "ARCHIVO A COMPRIMIR: "$ARCHIVO
	cd $DIRHISTO

	#echo $DIRHISTO
	echo "DIR DESTINO: "`pwd`

	tar -cvzf $ARCHIVO.gz $DIRLOGS/$ARCHIVO --remove-files

else
	echo "NO SE ENCOTRARON LOGS A COMPRIMIR"
fi


