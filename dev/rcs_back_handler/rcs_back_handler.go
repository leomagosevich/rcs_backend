package main

import (
	"bytes"
	"encoding/json"
	"fmt"

	//"log"
	"net/http"
	"os"
	"os/exec"
	"rcs_backend/conectarbd"
	"rcs_backend/controlenviadas"
	"rcs_backend/log"
	"rcs_backend/obtenerfechaactual"

	"github.com/gorilla/mux"
)

type Campania struct {
	ID string `json:"id,omitempty"`
}

var (
	usuario      string
	passwd       string
	host         string
	base         string
	idCamp       string
	campania     Campania
	concurrencia string
	ejBackend string
	err          error
)

func StartBroadcast(w http.ResponseWriter, req *http.Request) {

	req.ParseForm()

	for key, value := range req.Form {
		fmt.Printf("Se recibió la campaña %s = %s\n", key, value)

		idDeCampania := value[0]

		fmt.Printf("Handler - concurrencia: %s\n", concurrencia)

		rcsBackend := exec.Command(ejBackend, idDeCampania, usuario, passwd, host, base, concurrencia)
		out := bytes.NewBuffer([]byte{})

		rcsBackend.Stdout = out

		err := rcsBackend.Start()

		if err != nil {
			mensajeErr := fmt.Sprintf("RCS_BACKEND - Error en la ejecución de la campaña %s: %v\n", idDeCampania, err)
			fmt.Printf(mensajeErr)
			json.NewEncoder(w).Encode(mensajeErr)
		}

		rcsBackend.Wait()

		if rcsBackend.ProcessState.Success() {
			mensaje := out.String()
			fmt.Println(mensaje)
			json.NewEncoder(w).Encode(mensaje)
		} else {
			if out != nil {
				fmt.Println(out.String())
				json.NewEncoder(w).Encode(out.String())
			}
		}

	}

}

func StopBroadcast(w http.ResponseWriter, req *http.Request) {

	pathBstop := os.Getenv("RCS_PATH_STOP")
	fmt.Printf("Archivo Bstop: %s\n", pathBstop)
	req.ParseForm()

	for key, value := range req.Form {
		fmt.Printf("Stop Broadcast - Se recibió la campaña %s = %s\n", key, value)

		idDeCampania := value[0]

		//Conecto BD
		db, err := conectarbd.ConectarBD(usuario, passwd, host, base)

		if err != nil {
			fmt.Printf("Error al conectar la BD - %v\n", err)
			os.Exit(2)
		}

		defer db.Close()

		bstopParam := fmt.Sprintf("%s ", idDeCampania)

		fmt.Printf("Se va a detener la campaña %s en unos instantes\n", bstopParam)

		rcsBstop := exec.Command(pathBstop, bstopParam)
		out := bytes.NewBuffer([]byte{})

		rcsBstop.Stdout = out

		err = rcsBstop.Start()

		if err != nil {
			mensajeErr := fmt.Sprintf("RCS_BSTOP - Error en la ejecución del Stop de la campaña %s: %v\n", idDeCampania, err)
			fmt.Printf(mensajeErr)
			json.NewEncoder(w).Encode(mensajeErr)
		}

		rcsBstop.Wait()

		if rcsBstop.ProcessState.Success() {

			fecha := obtenerfechaactual.ObtenerFechaActual()

			err := log.Logueo(fmt.Sprintf("%s Se detuvo la Campaña %s", fecha, idDeCampania))
			if err != nil {
				fmt.Printf("Error al escribir en el log - %v\n", err)
			}

			cantidad, err := controlenviadas.ControlEnviadas(db, idDeCampania)

			if err != nil {
				fmt.Printf("Error al obtener cantidad de líneas enviadas - %v\n", err)
			}

			log.Logueo(fmt.Sprintf("%s Finaliza Campaña %s - Total de líneas enviadas: %d", fecha, idDeCampania, cantidad))

			if err != nil {
				fmt.Printf("Error al escribir en el log - %v\n", err)
				os.Exit(2)
			}

			fmt.Printf("Campaña %s detenida OK\n", idDeCampania)
			json.NewEncoder(w).Encode("OK")
		} else {
			if out.String() != "" {
				fmt.Println(out.String())
				json.NewEncoder(w).Encode("NO OK")
				fecha := obtenerfechaactual.ObtenerFechaActual()
				err := log.Logueo(fmt.Sprintf("%s Verificar la ejecución del stopBroadcast para la Campaña %s", fecha, idDeCampania))
				if err != nil {
					fmt.Printf("Error al escribir en el log - %v\n", err)
				}
			} else {
				fmt.Printf("Verificar la ejecución del stopBroadcast para la Campaña %s\n", idDeCampania)
				json.NewEncoder(w).Encode("NO OK")
				fecha := obtenerfechaactual.ObtenerFechaActual()
				err := log.Logueo(fmt.Sprintf("%s Verificar la ejecución del stopBroadcast para la Campaña %s", fecha, idDeCampania))
				if err != nil {
					fmt.Printf("Error al escribir en el log - %v\n", err)
				}
			}
		}

	}

}

func main() {

	usuario = os.Args[1]
	passwd = os.Args[2]
	host = os.Args[3]
	base = os.Args[4]
	concurrencia = os.Args[5]
	ejBackend = os.Args[6]

	if concurrencia == "true" {
		fmt.Printf("Concurrencia verdadero: %s\n", concurrencia)
	} else {
		fmt.Printf("Concurrencia falso: %s\n", concurrencia)
	}

	router := mux.NewRouter()

	router.HandleFunc("/send", StartBroadcast).Methods("POST")

	router.HandleFunc("/stop", StopBroadcast).Methods("POST")

	fmt.Println("Escuchando en el puerto 8180")

	err := http.ListenAndServe(":8180", router)

	if err != nil {
		fmt.Printf("No se pudo levantar el servicio - %v\n", err)
		err2 := log.Logueo(fmt.Sprintf("%sSe detuvo la Campaña %s", err))
		if err2 != nil {
			fmt.Printf("Error al escribir en el log - %v\n", err2)
		}
	}

}
