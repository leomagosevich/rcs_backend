PROYECTO RCS - REPOSITORIO DEL SERVICIO BACKEND CON CONCURRENCIA

Para ejecutar el servicio desde el código fuente utilizar el comando go run rcs_back_handler.go $MYSQL_USER $MYSQL_PASSWD $MYSQL_HOST $MYSQL_DB $RCS_CONCURRENCIA

Para ejecutar el servicio desde el ejecutable buildeado utilizar el comando ./rcs_back_handler $MYSQL_USER $MYSQL_PASSWD $MYSQL_HOST $MYSQL_DB $RCS_CONCURRENCIA

El archivo startBroadcast.sh incluye ambas posibilidades de ejecución, descomentar la deseada.

El archivo stopBroadcast.sh, encargado de la detención de campañas, es ejecutado desde el servicio rcs_back_handler recibiendo como parámetro el Bid de la campaña a detener. En el mismo se realiza un kill -9 del backend que está ejecutando la campaña en cuestión.

Tener en cuenta que al lanzar el servicio se pasan como argumentos los parámetros necesarios para realizar la conexión contra la BD y habilitar o no la ejecución concurrente. Tamboién se definen las rutas de ubicación del ejecutable rcs_backend, stopBackend.sh y carpeta de logueo. Para ello, configurar las variables de entorno correspondientes:

	export MYSQL_DB="mi_db"
	export MYSQL_USER="usuario"
	export MYSQL_PASSWD="pass"
	export MYSQL_HOST="tcp(ip_o_servicio:puerto)"
	export RCS_CONCURRENCIA="false"   # o true
	export RCS_PATH_BACKEND="./rcs_backend"
	export RCS_PATH_LOG="./logs/"
	export RCS_PATH_STOP="./stopBackend.sh"

En el directorio logs se creará un log diario con los datos relevantes de cada ejecución del servicio (start, restart y stop de camapañas)